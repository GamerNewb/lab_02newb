﻿using UnityEngine;
using System.Collections;
using UnityEditor;

/**
*@author Cody Newberry
*@date 9/6/2016
*@This script creates a prefab through the Menu Tool drop down
public class CreatePrefab : MonoBehaviour {

	[MenuItem("Project Tools/Create Prefab")]
	public static void PrefabCreation()
	{
		GameObject[] selectedObjects = Selection.gameObjects;

		foreach (GameObject go in selectedObjects) 
		{
			string name = go.name;
			string assetPath = "Assets/" + name + ".prefab";

			if(AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)))
				{
				//Debug.Log ("Asset exist!");
				if(EditorUtility.DisplayDialog("Caution", "Prefab " + name + " already exists. Do you want to overwrite?","Yes","No"))
					{
					//Debug.Log ("Overwriting!");
                    CreateNew(go, assetPath);
					}
				}
				else 
				{
				//Debug.Log ("Asset does not exist!");
                CreateNew(go, assetPath);
            }

			//Debug.Log ("Name: " + go.name + "Path: " + assetPath);
			
		}
	}

	public static void CreateNew(GameObject obj, string location)
	{
		Object prefab = PrefabUtility.CreateEmptyPrefab (location);
		PrefabUtility.ReplacePrefab (obj, prefab);
		AssetDatabase.Refresh ();

		DestroyImmediate (obj);
		GameObject clone = PrefabUtility.InstantiatePrefab (prefab) as GameObject;
	}
}
